// babel.config.js
module.exports = {
    plugins: [
        // '@babel/plugin-transform-runtime',
        //  ['@babel/plugin-proposal-decorators', { legacy: true }],
        // ['@babel/plugin-proposal-class-properties', { loose: true }],
        //  '@babel/plugin-syntax-dynamic-import',
        // '@babel/plugin-transform-flow-strip-types',
        // '@babel/plugin-transform-regenerator',
        // '@babel/plugin-proposal-export-default-from',
        // ['babel-plugin-react-native-platform-specific-extensions', {
        // 'extensions': ['js']
        // }]
        "@babel/plugin-transform-flow-strip-types",
        ["@babel/plugin-proposal-decorators", { "legacy": true }],
        ["@babel/plugin-proposal-class-properties", { "loose" : true }],
        "@babel/plugin-transform-regenerator",
        "@babel/plugin-transform-async-to-generator",
        "@babel/plugin-transform-runtime"
    ],
    presets: [
        //"module:react-native",
        // 'react-optimize',
        //'@babel/preset-env',
        'module:metro-react-native-babel-preset',
        //'@babel/preset-react',
        //'@babel/typescript',
    ],
};
