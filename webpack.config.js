// webpack.config.js
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
// import HtmlWebpackPlugin from "html-webpack-plugin";

module.exports = {
    // ...the rest of your config
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './public/bundle.js'
    },
    resolve: {
        modules:  [
            'src',
            'node_modules'
        ],
        extensions: ['.js'],
        alias: {
            //'react-native-screens': 'react-native-screens',
            'react-native': 'react-native-web'
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
            filename: './index.html',
            favicon: './public/favicon.ico',
            manifest: './public/manifest.json'
        })
    ],
    module: {
        rules: [
            {
                test: /\.(tsx|ts|jsx|js|mjs)$/,
                //exclude: /node_modules/,
                // loader: 'babel-loader',
                // query: {
                //     presets: ['react', 'es2015'],
                // },
                include: [
                    path.join(__dirname, '/src'),
                    path.join(__dirname, '/node_modules/react-native-screens'),
                    path.join(__dirname, '/node_modules/react-native-ratings'),
                    path.join(__dirname, '/node_modules/react-native-elements'),
                    path.join(__dirname, '/node_modules/react-native-vector-icons'),
                    path.join(__dirname, '/node_modules/react-native-gesture-handler'),
                    path.join(__dirname, '/node_modules/react-native-safe-area-view'),
                    path.join(__dirname, '/node_modules/react-native-svg'),
                    path.join(__dirname, '/node_modules/react-navigation'),
                    path.join(__dirname, '/node_modules/react-navigation-drawer'),
                    path.join(__dirname, '/node_modules/react-navigation-stack'),
                    path.join(__dirname, '/node_modules/react-navigation-tabs')
                ],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env', '@babel/react']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            // {
            //     test: /\.json$/,
            //     use: 'file-loader'
            // },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
        ],
    }
}
