package org.posline.app.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.UiThreadUtil;

import java.util.Objects;
import java.util.Random;
import java.util.prefs.Preferences;

import androidx.annotation.NonNull;

public class UniqueIdentifierModule extends ReactContextBaseJavaModule {

    public UniqueIdentifierModule(ReactApplicationContext context) {
        super(context);
    }

    @NonNull
    @Override
    public String getName() {
        return "UniqueIdentifierModule";
    }

    private int getRandomPositiveInt(Random r) {
        return r.nextInt() & 0x70000000;
    }

    private String generateAppInstanceId(SharedPreferences sharedPreferences){
        Random r = new Random();
        String appInstanceId = "PL"+getRandomPositiveInt(r)+""+getRandomPositiveInt(r);
        sharedPreferences.edit().putString("app_instance_id",appInstanceId).apply();
        System.out.println("generated new app instance id = "+appInstanceId);
        return appInstanceId;
    }

    @ReactMethod
    public void getAppInstanceId(Promise promise) {
        UiThreadUtil.runOnUiThread(() -> {
            try {
                SharedPreferences preferences = Objects.requireNonNull(
                        getCurrentActivity()).getSharedPreferences("app_id", Context.MODE_PRIVATE);
                String appInstanceId = preferences.getString("app_instance_id", null);
                if (appInstanceId == null) {
                    appInstanceId = generateAppInstanceId(preferences);
                }
                promise.resolve(appInstanceId);
            } catch (Exception e){
                promise.reject(e);
            }
        });
    }
}
