package org.posline.app.modules;

import android.app.Activity;
import android.view.WindowManager;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.UiThreadUtil;

import java.util.Objects;

import androidx.annotation.NonNull;

public class ScreenCaptureProtectionModule extends ReactContextBaseJavaModule {

    public ScreenCaptureProtectionModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @NonNull
    @Override
    public String getName() {
        return "ScreenCaptureProtectionModule";
    }

    private void set(boolean toState) {
        UiThreadUtil.runOnUiThread(() -> {
            try {
                Activity activity = Objects.requireNonNull(getCurrentActivity());
                if (toState) {
                    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
                } else {
                    activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
                }
                //promise.resolve("ok");
            } catch (Exception e) {
               // promise.reject(e);
            }
        });
    }

    @ReactMethod
    public void enable(@SuppressWarnings("unused") Callback onScreenshotCapturedCallback) {
        set(true);
    }

    @ReactMethod
    public void disable(Promise promise) {
        set(false);
    }
}
