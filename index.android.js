/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './src/App.json';

// Navigation.registerComponent(
//     'home_scene')
// Navigation.events().registerAppLaunchedListener(() => {
//     Navigation.setRoot({
//         root: {
//             stack: {
//                 children: [
//                     {
//                         component: {
//                             name: 'home_scene'
//                         }
//                     }
//                 ]
//             }
//         }
//     })
// });
AppRegistry.registerComponent(appName, () => App);
