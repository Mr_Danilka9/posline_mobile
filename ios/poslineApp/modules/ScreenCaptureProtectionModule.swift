//import UIScreen
//import Notification
//import NotificationCenter
//import NSObject
//import RCTPromiseResolveBlock
//import RCTPromiseRejectBlock
import UIKit
import NotificationCenter
// import JavaScriptCore
//
// var globalvar: String = "PIZDA"

@objc(ScreenCaptureProtectionModule)
class ScreenCaptureProtectionModule: NSObject {
  
  var currentCallback: RCTResponseSenderBlock?;

  override init(){
super.init();
    currentCallback = nil;
}

  @objc
  func screenChangedEvent(){
    if #available(iOS 11.0, *) {
      if UIScreen.main.isCaptured == true{
        print("captured")
        currentCallback!([NSNull(), "aaaaaaaaaaaaaaa"])
//         let callback : String = "aaaa"
//
        //blank the view..
        //             let testFrame : CGRect = CGRect(x: 0, y: 200, width: 320, height: 200)
        //             let testView : UIView = UIView(frame: testFrame)
        //             testView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        //             testView.alpha=0.5
        //             self.view.addSubview(testView)

      }
      else{
        print("Screen")
      }
    } else {
      // Fallback on earlier versions
    }
  }
//
//   func request(requestPath:String, requestParams:Any, callback:((Result) -> Void)) {
//     if let value = jsHandler.callFunction(functionName: "getPriceAndDiscountOfProduct", withData: product, type:Product.self) {
//       if value.isObject,
//         let dictionary = value.toObject() as? [String:Any] {
//         let price = dictionary["price"] as? Double ?? 0.0
//         let discount = dictionary["discount"] as? Double ?? 0.0
//         return (price, discount)
//       }
//       else {
//         print("error while getting price and discount for \(product.name)")
//       }
//     }
//}


  func userGetId() -> String{
    let defaults = UserDefaults.standard
    let key = defaults.object(forKey: "unique_app_id") as? String ?? String()
    return key
  }

  @objc(enable:)
  func enable(_ callback:@escaping RCTResponseSenderBlock) -> Void{
    print("enabled")
    currentCallback = callback;
    NotificationCenter.default.addObserver(self, selector:#selector(screenChangedEvent), name:UIApplication.userDidTakeScreenshotNotification, object: nil)
    currentCallback!([NSNull(), "aaaaaaaaaaaaaaa"])
//     if UIApplication.userDidTakeScreenshotNotification == true{
//       globalvar = "Screenshotted"
//     }
//

  }

  @objc(disable)
  func disable() -> Void{
    print("disabled")
    NotificationCenter.default.removeObserver(self, name:UIApplication.userDidTakeScreenshotNotification, object: nil)
  }

}
//class UIAlertController : UIViewController{
//  func doAlert(){
//    let alert = UIAlertController(title: "Предупреждение!", message: "Просим больше не делать скриншоты экрана и не нарушить авторские права продукта.", preferredStyle: .alert)
//    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"),
//                                  style: .default, handler: { _ inNSLog("The \"OK\" alert occured.")}))
//    self.present(alert, animated: true, completion: nil)
//  }
//
//}
