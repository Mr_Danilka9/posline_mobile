// ScreenCaptureProtectionModule.m
//#import "ScreenCaptureProtectionModule.h"
#import "../poslineApp-Bridging-Header.h"

//@implementation ScreenCaptureProtectionModule

@interface RCT_EXTERN_MODULE(ScreenCaptureProtectionModule, NSObject)

RCT_EXTERN_METHOD(enable: (RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(disable)

@end
// To export a module named ScreenCaptureProtectionModule
// RCT_EXPORT_MODULE();
//
// RCT_REMAP_METHOD(enable, loadVideoWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
// {
//   BOOL screenshoted = true; // could be any data type listed under https://facebook.github.io/react-native/docs/native-modules-ios.html#argument-types
//   if (screenshoted) {
//     resolve("ok");
//   } else {
//     reject("error");
//   }
// }
//
// RCT_REMAP_METHOD(disable, loadVideoWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
// {
//   BOOL screenshoted = false;
//   if (screenshoted) {
//       resolve("ok");
//     } else {
//       reject("error");
//     }
// }
//
// NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
// [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationUserDidTakeScreenshotNotification
//                                                   object:nil
//                                                    queue:mainQueue
//                                               usingBlock:^(NSNotification *note) {
//                                                  // executes after screenshot
//                                               }];
//
// @end
