@objc(ScreenCaptureProtectionModule)
class ScreenCaptureProtectionModule: NSObject {

@objc
func screenChangedEvent(){
        if UIScreen.main.isCaptured == true{
            print("captured")
            //blank the view..
//             let testFrame : CGRect = CGRect(x: 0, y: 200, width: 320, height: 200)
//             let testView : UIView = UIView(frame: testFrame)
//             testView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
//             testView.alpha=0.5
//             self.view.addSubview(testView)
        }
        else{
        print("Screen")
        }
    }

@objc(enable:rejecter:)
func enable(_ resolve:PRCTPromiseResolveBlock, rejecter reject:RCTPromiseRejectBlock) -> Void{
    print("enabled")
    NotificationCenter.default.addObserver(self, selector:#selector(screenChangedEvent), name:Notification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
    resolve("ok")
}

@objc(disable:rejecter:)
func disable(_ resolve:PRCTPromiseResolveBlock, rejecter reject:RCTPromiseRejectBlock) -> Void{
    print("disabled")
    NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
    resolve("ok")
}

}
