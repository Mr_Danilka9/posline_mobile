import React, {Component} from 'react';
import {NativeModules} from 'react-native';

export default class ScreenCaptureProtectionModule {

    constructor() {
        this.enableScreenCaptureProtection = this.enableScreenCaptureProtection.bind(this);
        this.disableScreenCaptureProtection = this.disableScreenCaptureProtection.bind(this);
        this.onScreenshotCaptured = this.onScreenshotCaptured.bind(this);
    }

    enableScreenCaptureProtection(onScreenshotTakenCallback, onInternetConnectionLostCallback) {
        // disable screenshoting on android, enable screenshot reports on ios
        try {
            NativeModules.ScreenCaptureProtectionModule.enable((error, data ) => {
                console.log("hui hui hui");
            })
        } catch (e) {
            console.log(e);
        }
        // establish connection with server
    }

    disableScreenCaptureProtection() {
        // disable security
        try {
            NativeModules.ScreenCaptureProtectionModule.disable()
        } catch (e) {
            console.log(e);
        }
        // broke web-socket connection
    }

    onScreenshotCaptured(error, data) {
        console.log("SCREENSHOT !!!!");
    }

}
