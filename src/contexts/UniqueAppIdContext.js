import React from 'react';
//import {Platform, Settings} from "react-native";
import DefaultPreference from 'react-native-default-preference';

const UniqueAppIdContext = React.createContext(null)

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function generateUniqueAppId(){
    let a1 = getRandomInt(1000000000,2000000000);
    let a2 = getRandomInt(1000000000,2000000000);
    let a3 = getRandomInt(1000000000,2000000000);
    let a4 = getRandomInt(1000000000,2000000000);
    return "PL"+a1+""+a2+""+a3+""+a4;
}

//if (Platform.OS === "android") {
export function asyncGetUniqueAppId(idConsumer) {
    DefaultPreference.get('unique_app_id').then(function(appId) {
        if (appId == null) {
            appId = generateUniqueAppId();
            DefaultPreference.set('unique_app_id', appId);
        }
        idConsumer(appId);
    })
    // if (Platform.OS === 'android') {
    //     const SharedPreferences = require('react-native-shared-preferences');
    //     SharedPreferences.setName("plankton_preferences")
    //     SharedPreferences.getItem('unique_app_id', function (appId) {
    //         if (appId == null) {
    //             appId = generateUniqueAppId();
    //             //console.log("generated unique app id = "+appId);
    //             SharedPreferences.setItem('unique_app_id', appId);
    //         }
    //         //console.log("returning unique app id = "+appId);
    //         idConsumer(appId)
    //     });
    // } else {
    //     let appId = Settings.get('unique_app_id');
    //     if (appId == null) {
    //         appId = generateUniqueAppId();
    //         //console.log("generated unique app id = "+appId);
    //         Settings.set('unique_app_id', appId);
    //     }
    //     //console.log("returning unique app id = "+appId);
    //     idConsumer(appId)
    // }
    // if (appId === null) {
    //     appId = generateUniqueAppId();
    //     console.log("generated unique app id = "+appId)
    //     Preference.set('unique_app_id',appId);
    // }
    // return appId;
}

export default UniqueAppIdContext;
