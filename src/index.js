/**
 * @format
 */

import {AppRegistry} from 'react-native';
import ReactDOM from 'react-dom';
import App from './App';
import {name as appName} from './App.json';
import React from "react";

//AppRegistry.registerComponent(appName, () => App);
ReactDOM.render(<App/>, document.getElementById('root'));
