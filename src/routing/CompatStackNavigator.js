import React, {Component} from "react";
import {Route} from "react-router";
import {AnimatedSwitch} from 'react-router-transition';
import {HashRouter} from "react-router-dom";
import "./CompatStackNavigator.web.css";

export default class CompatStackNavigator extends Component {
    constructor(props) {
        super(props);
        this.routes = this.props.scenes.map(scene => {
            scene.key = scene.name;
            scene.path = "/" + scene.name;
            return React.createElement(Route, {
                key: this.props.key + ":" + scene.key + "_route",
                path: scene.path,
                component: scene.component,
                //onEnter:
                // onChange: (nextState, replace, callback) => {
                //
                // }
            }, null);
        })
        this.routes.push(React.createElement(Route, {
            key: this.props.key + ":default_route",
            component: this.props.scenes[0].component
        }, null));
        console.log(this.props.scenes);
    }

    render() {
        return React.createElement(HashRouter, null,
            React.createElement(AnimatedSwitch, {
                atEnter: {
                    opacity: 0,
                    top: 100,
                },
                atLeave: {
                    opacity: 0,
                    top: -200,
                },
                atActive: {
                    opacity: 1,
                    top: 0
                },
                className: "switch-wrapper"
            }, this.routes));
    }
}


