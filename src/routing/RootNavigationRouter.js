import React, {Component} from "react";
import CodeInputFragment from "../scenes/CodeInputFragment";
import HomeScreenFragment from "../scenes/HomeScreenFragment";
import CompatStackNavigator from "./CompatStackNavigator";
import PDFViewFragment from "../scenes/PDFViewFragment";
import ContentViewFragment from "../scenes/ContentViewFragment";
import LocaleStrings from "../localization"

class RootNavigationRouter extends Component {
    render() {
        return (
            <CompatStackNavigator scenes={[
                {
                    name: "home_scene",
                    component: HomeScreenFragment,
                    showActionBar: false,
                    title: LocaleStrings.Scenes.HomeScene,
                },
                {
                    // first scene is a default scene
                    name: "code_input_scene",
                    component: CodeInputFragment,
                    showActionBar: false,
                    title: LocaleStrings.Scenes.LoginScene,
                },
                {
                    name: "code_content_scene",
                    component: ContentViewFragment,
                    showActionBar: false,
                    title: LocaleStrings.Scenes.ContentViewFragment,
                },
                {
                    name: "pdf_view_scene",
                    component: PDFViewFragment,
                    showActionBar: true,
                    title: LocaleStrings.Scenes.PdfViewer,
                }
            ]}/>
        )
    }
}

export default RootNavigationRouter;
