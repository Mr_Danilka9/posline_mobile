import React, {Component} from "react";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";

export default class CompatStackNavigator extends Component {

    constructor(props) {
        super(props);
        this.navigator = createStackNavigator();
        this.screens = [];
        this.screens = this.props.scenes.map(scene => {
            scene.path = "/" + scene.name
            scene.options = {}
            return React.createElement(this.navigator.Screen, {
                key: this.props.key+":"+scene.name+"_route",
                name: scene.name,
                component: scene.component,
                options: {
                    headerShown: scene.showActionBar ? scene.showActionBar : false,
                    title: scene.title ? scene.title : scene.name,
                },
            });
        })
        console.log(this.props.scenes);
    }

    render() {
         return React.createElement(NavigationContainer, null,
            React.createElement(this.navigator.Navigator, {
                mode: "none"
            }, this.screens));
    }
}
