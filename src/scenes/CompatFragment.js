import {Component} from "react";
import CompatFragmentLifecycleStrategy from "./CompatFragmentLifecycleStrategy";

class CompatFragment extends Component {

    constructor(props, context, updater) {
        super(props, context, updater);
        this.history = props.history;
        this.navigation = props.navigation;
        this.gotoScene = this.gotoScene.bind(this);
        this.doCreate = this.doCreate.bind(this);
        this.doResume = this.doResume.bind(this);
        this.doPause = this.doPause.bind(this);
        this.doDestroy = this.doDestroy.bind(this);
        this.onCreate = this.onCreate.bind(this);
        this.onResume = this.onResume.bind(this);
        this.onPause = this.onPause.bind(this);
        this.lifecycle = new CompatFragmentLifecycleStrategy(this);
        this.state = {
            doTraceLifecycle: false,
            isCreated: false
        };
    }

    componentDidMount(): void {
        if (!this.state.isCreated) {
            this.doCreate()
            this.setState({
                isCreated: true
            })
        } else {
            if (!this.lifecycle.dispatchFragmentDidMount()) {
                this.doResume()
            }
        }
    }

    componentWillUnmount(): void {
        if (!this.lifecycle.dispatchFragmentWillUnmount()) {
            this.doPause()
        }
    }

    doCreate() {
        try {
            this.onCreate()
            if (this.state.doTraceLifecycle) {
                console.log("fragment: onCreate")
            }
        } catch (e) {
            console.log("fragment: onCreate error")
            console.log(e)
        }
    }

    doResume() {
        try {
            this.onResume()
            if (this.state.doTraceLifecycle) {
                console.log("fragment: onResume")
            }
        } catch (e) {
            console.log("fragment: onResume error")
            console.log(e)
        }
    }

    doPause() {
        try {
            this.onPause()
            if (this.state.doTraceLifecycle) {
                console.log("fragment: onPause")
            }
        } catch (e) {
            console.log("fragment: onPause error")
            console.log(e)
        }
    }

    doDestroy() {
        try {
            if (this.state.doTraceLifecycle) {
                console.log("fragment: onDestroy")
            }
        } catch (e) {
            console.log("fragment: onDestroy error")
            console.log(e)
        }
    }

    onCreate() {
    }

    onResume() {
    }

    onPause() {
    }

    gotoScene(name, params) {
        this.lifecycle.gotoScene(name, params);
    }
}

export default CompatFragment;
