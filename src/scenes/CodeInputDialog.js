import React, {Component} from 'react';
import {Text, TextInput, StyleSheet} from 'react-native'
import {ActivityIndicator, Button, Headline, Portal, Dialog, Paragraph} from 'react-native-paper';
import UniqueAppIdContext from "../contexts/UniqueAppIdContext";
import {registerCode} from "../api/Methods"
import LocaleStrings from "../localization"

export default class CodeInputDialog extends Component {

    static contextType = UniqueAppIdContext

    constructor(props, context, updater) {
        super(props, context, updater);
        this.inlineRenderCurrentState = this.inlineRenderCurrentState.bind(this);
        this.onDismissPressed = this.onDismissPressed.bind(this);
        this.onActionPressed = this.onActionPressed.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
        this.onFailure = this.onFailure.bind(this);
        this.onDismiss = this.onDismiss.bind(this);
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
        this.state = {
            isVisible: false,
            isFetching: false,
            isSuccess: false,
            isFailure: false,
            inputCode: "",
        }
    }

    show() {
        this.setState({
            isFetching: false,
            isSuccess: false,
            isFailure: false,
            isVisible: true,
            inputCode: ""
        })
    }

    hide() {
        this.setState({
            isVisible: false,
        })
    }

    onDismiss() {
        this.hide();
        if (this.props.onDismissCallback) {
            this.props.onDismissCallback();
        }
        return true;
    }

    onSuccess() {
        this.setState({
            isFetching: false,
            isSuccess: true
        })
        if (this.props.onSuccessCallback) {
            this.props.onSuccessCallback();
        }
    }

    onFailure() {
        this.setState({
            isFetching: false,
            isFailure: true
        })
        if (this.props.onFailureCallback) {
            this.props.onFailureCallback();
        }
    }

    onDismissPressed() {
        this.onDismiss()
    }

    async onActionPressed() {
        this.setState({
            isSuccess: false,
            isFailure: false,
            isFetching: true,
        })
        if (this.state.inputCode === "") {
            this.setState({
                isFetching: false,
                isFailure: true,
                isSuccess: false,
            })
            return
        }
        registerCode(this.context, this.state.inputCode).then(
            this.onSuccess,
            this.onFailure
        );
    }

    render() {
        return (
            <Portal>
                <Dialog visible={this.state.isVisible}
                        onDismiss={this.onDismiss}>
                    <Dialog.Content style={styles.contentContainerStyle}>
                        <Headline style={styles.titleStyle}>{LocaleStrings.CodeInput.Title}</Headline>
                        <Paragraph style={styles.descriptionStyle}>{LocaleStrings.CodeInput.Description}</Paragraph>
                        <TextInput onChangeText={t => this.setState({inputCode: t})}
                                   placeholder={LocaleStrings.CodeInput.InputPlaceholder}
                                   underlineColorAndroid={"#00000000"}
                                   style={styles.codeInputStyle}
                                   textAlign={"center"}
                                   multiline={false}
                                   value={this.state.inputCode}/>
                        {this.inlineRenderCurrentState()}
                        <Button contentStyle={styles.codeSubmitButtonContentStyle}
                            style={styles.codeSubmitButtonStyle}
                            onPress={this.onActionPressed}
                            mode={"contained"}>{LocaleStrings.CodeInput.SubmitCode}</Button>
                    </Dialog.Content>
                </Dialog>
            </Portal>
        )
    }

    inlineRenderCurrentState() {
        if (this.state.isSuccess) {
            // return (
            //     <Text style={{
            //         //backgroundColor: "darkgreen",
            //         color: "darkgreen"
            //     }}>Content added!</Text>
            // )
        }
        if (this.state.isFailure) {
            return (
                <Text style={styles.failureTextStyle}>{LocaleStrings.CodeInput.FailureText}</Text>
            )
        }
        if (this.state.isFetching) {
            return (
                <ActivityIndicator animating={true}
                                   color={"blue"}/>
            )
        }
    }
}

const styles = StyleSheet.create({
    contentContainerStyle: {
        display: "flex",
        alignItems: "center"
    },
    titleStyle: {},
    descriptionStyle: {
        textAlign: "center"
    },
    codeInputStyle: {
        maxHeight: 64,
        margin: 16,
        width: 192,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: "#2b7a78",
        fontSize: 18
    },
    codeSubmitButtonStyle: {
        width: 192,
        margin: 8,
        padding: 0,
        fontSize: 12,
        height: 36,
        borderRadius: 17,
        backgroundColor: "#2b7a78",
    },
    codeSubmitButtonContentStyle: {
        height: 36,
        margin: 0,
        padding: 0,
    },
    failureTextStyle: {
        //backgroundColor: "red",
        color: "red"
    }
})
