import React from 'react'

export default class CompatFragmentLifecycleStrategy {
    constructor(fragment) {
        this.fragment = fragment;
        this.fragment.props.navigation.addListener('focus', () => this.focusCapturedEventCallback(this))
        this.fragment.props.navigation.addListener('blur', () => this.focusLostEventCallback(this))
        this.gotoScene = this.gotoScene.bind(this);
    }

    gotoScene(name: string, params: object) {
        this.fragment.navigation.navigate(name, params);
    }

    focusCapturedEventCallback(strategy) {
        this.fragment.doResume()
    }

    focusLostEventCallback(strategy) {
        this.fragment.doPause()
    }

    dispatchFragmentDidMount() {
        return true;
    }

    dispatchFragmentWillUnmount() {
        return true;
    }
}
