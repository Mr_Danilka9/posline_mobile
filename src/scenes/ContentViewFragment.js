import React from 'react';
import axios from 'axios';
import CompatFragment from './CompatFragment';
import {StatusBar, Text, Button} from 'react-native';
//port {Button} from 'react-native-elements';
import CompatSafeAreaView from '../widgets/CompatSafeAreaView';
import ContentItemsList from '../widgets/ContentItemsList';
import UniqueAppIdContext from "../contexts/UniqueAppIdContext";
import {getCodeContent} from "../api/Methods";


export default class ContentViewFragment extends CompatFragment {

    static contextType = UniqueAppIdContext
    constructor(props, context, updater) {
        super(props, context, updater);
        this.onGoToLibraryPressed = this.onGoToLibraryPressed.bind(this);
        this.loadContentByCode = this.loadContentByCode.bind(this);
        this.state = {
            appId: this.context,
            codeValue: this.props.route.params.codeValue,
            loading: false,
            listData: []
        }
        console.log(this.state.appId)
    }

    onCreate() {
        super.onCreate();
        this.loadContentByCode()
    }

    async loadContentByCode() {
        this.setState({
            loading: true
        })
        const itemsData = (await getCodeContent(this.context,this.state.codeValue)).data;
        //TODO: fetch content by code
        // let itemsData = []
        // for (let i = 0; i < 10; i++) {
        //     itemsData.push({id: "" + i, key: "" + i, title: "card " + i})
        // }
        console.log(itemsData);
        this.setState({
            loading: false,
            listData: itemsData
        })
        if (itemsData.length === 1) {
            super.gotoScene('pdf_view_scene', {
                contentId: itemsData[0].id,
                //TODO: set real code
                //codeValue: 'some_value'
            })
        }
    }

    onListItemClicked(item) {
        console.log("clicked " + item.title);
        super.gotoScene('pdf_view_scene',{
            contentId: item.id,
        })
    }

    onGoToLibraryPressed() {
        //we should be on this fragment after

    }

    render() {
        return (
            <React.Fragment>
                <StatusBar
                    backgroundColor="#5a72db"
                    translucent={true}/>
                <CompatSafeAreaView>
                    <Text>I am content view fragment</Text>
                    <ContentItemsList
                        listData={this.state.listData}
                        itemCallbacks={this}/>
                    <Button
                        onPress={this.onGoToLibraryPressed}
                        title={"Go back to library"}/>
                </CompatSafeAreaView>
            </React.Fragment>
        )
    }
}
