import React, {Component} from "react";
import UniqueAppIdContext from "../contexts/UniqueAppIdContext";
import {Button, Dialog, Headline, Paragraph, Portal} from "react-native-paper";
import LocaleStrings from "../localization";
import {StyleSheet, TextInput} from "react-native";

export default class ScreenshotViolationDialog extends Component {

    static contextType = UniqueAppIdContext

    constructor(props, context, updater) {
        super(props, context, updater);
        this.onDismiss = this.onDismiss.bind(this);
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
        this.state = {
            isVisible: false,
            warnData: {
                title: "",
                description: "",
            }
        }
    }


    show() {
        this.setState({
            isVisible: true,
        })
    }

    hide() {
        this.setState({
            isVisible: false,
        })
    }

    onDismiss() {
        if (this.props.onDismissCallback) {
            this.props.onDismissCallback();
        }
        return true;
    }

    render() {
        return (
            <Portal>
                <Dialog visible={this.state.isVisible}
                        dismissable={true}>
                    <Dialog.Title>{this.state.warnData.title}</Dialog.Title>
                    <Dialog.Content style={styles.contentContainerStyle}>
                        <Paragraph style={styles.descriptionStyle}>{this.state.warnData.description}</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={this.onDismiss}>OK</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        )
    }
}


const styles = StyleSheet.create({
    contentContainerStyle: {
        display: "flex",
        alignItems: "center"
    },
    titleStyle: {},
    descriptionStyle: {
        textAlign: "center"
    },
    codeInputStyle: {
        maxHeight: 64,
        margin: 16,
        width: 192,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: "#2b7a78",
        fontSize: 18
    },
    codeSubmitButtonStyle: {
        width: 192,
        margin: 8,
        padding: 0,
        fontSize: 12,
        height: 36,
        borderRadius: 17,
        backgroundColor: "#2b7a78",
    },
    codeSubmitButtonContentStyle: {
        height: 36,
        margin: 0,
        padding: 0,
    },
    failureTextStyle: {
        //backgroundColor: "red",
        color: "red"
    }
})
