import React from "react";
import CompatFragment from "./CompatFragment";
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Button,
    StatusBar, SafeAreaView,
} from 'react-native';
//import {Button} from 'react-native-elements'
import {NativeModules} from 'react-native';
import CompatSafeAreaView from "../widgets/CompatSafeAreaView";
import UniqueAppIdContext from "../contexts/UniqueAppIdContext";
import {registerCode} from "../api/Methods"



class CodeInputFragment extends CompatFragment {

    static contextType = UniqueAppIdContext
    constructor(props, context, updater) {
        super(props, context, updater);
        this.onCodeInputValueChanged = this.onCodeInputValueChanged.bind(this);
        this.state = {
            codeValue: ""
        }
    }

    render(): React.ReactNode {
        return (
            <React.Fragment>
                <StatusBar backgroundColor="#5a72db" translucent={true}/>
                <CompatSafeAreaView style={styles.container}>
                    <View style={styles.content}>
                        <Text style={styles.promptTitle}>Put your code below</Text>
                        <TextInput
                            onChangeText={this.onCodeInputValueChanged}
                            style={styles.codeInput}
                            hint="input you code here"
                            value={this.state.codeValue}/>
                        <Button
                            style={styles.enterButton}
                            title="enter the room"
                            onPress={this.onEnterButtonPressed.bind(this)}/>
                    </View>
                </CompatSafeAreaView>
            </React.Fragment>
        )
    }

    onResume() {
        super.onResume();
        // try {
        //     NativeModules.ScreenCaptureProtectionModule.enable()
        // } catch (e) {
        //     console.log(e);
        // }
    }

    onPause() {
        super.onPause();
        // try {
        //     NativeModules.ScreenCaptureProtectionModule.disable();
        // } catch (e) {
        //     console.log(e);
        // }
    }

    onCodeInputValueChanged(value) {
        this.setState({
            codeValue: value
        });
    }

    async onEnterButtonPressed() {
        (await registerCode(this.context,this.state.codeValue)).data;
        super.gotoScene('home_scene');
        // if ("" === this.state.codeValue) {
        //     super.gotoScene('home_scene');
        // } else {
        //     (await registerCode(this.context,this.state.codeValue)).data);
        //     super.gotoScene('code_content_scene', {
        //         codeValue: this.state.codeValue
        //     });
        // }
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#5a72db",
        width: "100%",
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    content: {
        backgroundColor: "white",
        width: 300,
        elevation: 1,
        padding: 8
    },
    promptTitle: {
        fontSize: 18,
        paddingTop: 16,
        paddingBottom: 8,
    },
    codeInput: {
        fontSize: 18,
        marginTop: 8,
        marginBottom: 8
    },
    // enterButton: {
    //     backgroundColor: "white",
    //     color: "black",
    //     marginTop: 8,
    // }
})

export default CodeInputFragment;
