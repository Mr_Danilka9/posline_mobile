import React, {Component} from 'react';
import CompatFragment from './CompatFragment'
import ContentItemsList from '../widgets/ContentItemsList'
import {StyleSheet, View, Text, StatusBar} from 'react-native';
import {FAB, Appbar} from 'react-native-paper';
import CompatSafeAreaView from '../widgets/CompatSafeAreaView';
import CodeInputDialog from './CodeInputDialog'
import UniqueAppIdContext from '../contexts/UniqueAppIdContext';
import {getLibraryContent} from '../api/Methods'
import LocaleStrings from "../localization"
import {Svg,Path} from "react-native-svg";

class HomeScreenFragment extends CompatFragment {

    static contextType = UniqueAppIdContext

    constructor(props, context, updater) {
        super(props, context, updater);
        this.onAddNewContentByCodePressed = this.onAddNewContentByCodePressed.bind(this);
        //this.refreshLibraryList = this.refreshLibraryList.bind(this);
        this.onListItemClicked = this.onListItemClicked.bind(this);
        this.loadLibraryList = this.loadLibraryList.bind(this);
        this.state = {
            loading: false,
            refreshing: true,
            listData: []
        }
    }

    onCreate() {
        super.onCreate();
        this.loadLibraryList();
    }

    onResume() {
    }

    async loadLibraryList() {
        this.setState({
            refreshing: false,
            loading: true
        });
        const itemsData = (await getLibraryContent(this.context)).data;
        this.setState({
            refreshing: false,
            loading: false,
            listData: itemsData
        });
        console.log("list loaded, size " + itemsData.length);
    }

    async refreshLibraryList() {
        this.setState({
            refreshing: true,
            loading: true,
        })
        const itemsData = (await getLibraryContent(this.context)).data;
        this.setState({
            refreshing: false,
            loading: false,
            listData: itemsData
        });
        console.log("list refreshed, size " + itemsData.length);
    }

    render() {
        return (
            <React.Fragment>
                <StatusBar backgroundColor={"#2b7a78"} translucent={true}/>
                <CompatSafeAreaView style={styles.rootStyle}>
                    {this.inlineRenderActionBar1()}
                    <View style={styles.listStackContainerStyle}>
                        {/*{this.inlineRenderSwipeToRefreshHint()}*/}
                        {this.inlineRenderEmptyListHint()}
                        {this.inlineRenderList()}
                    </View>
                    {/*{this.renderAddNewContentByCode()}*/}
                </CompatSafeAreaView>
                <CodeInputDialog
                    ref={'codeInputDialog'}
                    onSuccessCallback={() => {
                        this.refs.codeInputDialog.hide();
                        this.refreshLibraryList();
                    }}/>
            </React.Fragment>
        )
    }


    inlineRenderActionBar() {
        return (
            <Appbar.Header
                style={styles.actionBarStyle}>
                <Appbar.Content
                    title={LocaleStrings.HomeTitle}/>
                <Appbar.Action
                    onPress={this.onAddNewContentByCodePressed}
                    icon={"plus"}/>
            </Appbar.Header>
        )
    }

    inlineRenderActionBar1() {
        return (
            <Appbar.Header style={styles.actionBarStyle}>
                <Appbar.Action color={"transparent"}
                    style={{visibility: "hidden"}}
                    //onPress={() => {console.log("account")}}
                    icon={'account'}/>
                <Appbar.Content title={<Text style={{}}> {LocaleStrings.HomeTitle} </Text>}
                    style={{ alignItems: 'center' }}/>
                <Appbar.Action
                    onPress={this.onAddNewContentByCodePressed}
                    icon={"plus"}/>
            </Appbar.Header>
        )
    }

    inlineRenderSwipeToRefreshHint() {
        if (!this.state.refreshing && this.state.listData.length === 0) {
            return (
                <View style={styles.listBackgroundRefreshHintStyle}>
                    <Text style={{}}>{LocaleStrings.ContentListHints.SwipeDownToRefresh}</Text>
                </View>
            )
        }
    }

    inlineRenderEmptyListHint(){
        if (!(this.state.loading && !this.state.refreshing) && this.state.listData.length === 0) {
            return (
                <View style={styles.listBackgroundEmptyHintStyle}>
                    <View style={{
                        width: "100%",
                        //backgroundColor: "red",
                        display: "flex",
                        flexDirection: "row-reverse",
                        marginTop: 16,
                        marginBottom: 16,
                    }}>
                        <HintArrowImage/>
                    </View>
                    <Text style={{
                        textAlign: "center",
                        fontSize: 16,
                        width: "64%",
                    }}>{LocaleStrings.ContentListHints.EmptyListHint}</Text>
                </View>
            )
        }
    }

    inlineRenderList() {
        return (
            <ContentItemsList
                onRefreshCallback={() => {this.refreshLibraryList()}}
                refreshing={this.state.refreshing}
                listData={this.state.listData}
                itemCallbacks={this}/>
        )
    }

    renderAddNewContentByCode() {
        return (
            <FAB
                onPress={this.onAddNewContentByCodePressed}
                style={styles.addContentFabStyle}
                icon={"plus"}
            />
        )
    }

    onAddNewContentByCodePressed() {
        this.refs.codeInputDialog.show();
        //super.gotoScene('code_input_scene');
    }

    // onReturnToLoginPressed() {
    //     super.gotoScene('code_input_scene');
    // }

    onListItemClicked(item) {
        console.log("clicked " + item.title);
        super.gotoScene('pdf_view_scene', {
            title: item.name,
            item: item,
            contentId: item.id,
        })
    }
}

class HintArrowImage extends Component{
    render() {
        return (
            <Svg width={161} height={217}>
                <Path
                    d={"m 119.12993,64.8045 c -1.6773,0.6837 -3.3731,1.3092 -5.0308,2.0675 -4.5529,2.0817 -8.8829,4.8024 -13.0402,7.9938 -0.2685,0.2062 -0.5894,0.5382 -0.859,0.2193 -0.343804,-0.4096 0.0405,-0.7392 0.1883,-1.0761 2.2101,-4.9767 4.4383,-9.9381 6.6253,-14.9331 3.4052,-7.7749 6.5661,-15.7449 9.5124,-23.8656 1.9991,-5.5109 3.8952,-11.1073 5.4543,-16.9025 1.3878,-5.1529 2.6176,-10.37772 3.3007,-15.84341 0.0522,-0.41966 -0.3097,-1.24426 0.269,-1.25326 0.5081,-0.0076 0.434,0.88583 0.5237,1.31581 1.3487,6.47909 3.1187,12.77806 4.8604,19.09206 2.3462,8.5097 4.9305,16.9043 7.5592,25.2695 3.2221,10.251 6.5193,20.4607 9.98546,30.5765 0.17575,0.5167 0.70654,1.2475 0.21446,1.6767 -0.39781,0.347 -0.70481,-0.4945 -1.0376,-0.885 -3.01032,-3.5242 -6.19822,-6.683 -9.76412,-9.0899 -2.164,-1.4603 -4.3909,-2.6621 -6.6468,-3.786 -0.7856,-0.3933 -1.0497,-0.0486 -1.0542,0.9392 -0.0436,8.2204 -1.0563,16.2151 -2.3009,24.1625 -1.1338,7.2397 -2.7194,14.2755 -4.4527,21.2475 -1.1216,4.515 -2.405,8.937 -3.77,13.306 -1.8178,5.819 -3.8725,11.482 -6.0648,17.052 -1.9632,4.993 -4.1537,9.79 -6.4221,14.513 -1.5466,3.218 -3.1879,6.367 -4.9867,9.344 -1.5338,2.537 -2.935004,5.237 -4.562004,7.654 -3.8605,5.735 -7.8876,11.236 -12.3776,16.111 -3.9501,4.288 -8.0954,8.168 -12.5232,11.5 -4.1985,3.158 -8.5999,5.758 -13.1789,7.848 -2.7704,1.265 -5.5874,2.336 -8.4584,3.142 -2.377,0.668 -4.761,1.336 -7.208,1.537 -0.467,0.037 -1.044,0.842 -1.347,0.103 -0.27,-0.666 0.506,-0.989 0.798,-1.464 1.794,-2.908 3.808,-5.569 5.047,-9.067 0.747,-2.111 1.253,-4.219 0.657,-6.65 -0.623,-2.541 -2.154,-3.783 -3.764,-4.818 -2.393,-1.537 -4.962,-2.217 -7.539,-2.643 -2.559,-0.42 -5.121,-0.777 -7.701,-0.855 -3.897,-0.116 -7.784,-0.05 -11.675,-0.034 -2.995,0.012 -5.96,0.353 -8.9449996,0.414 -0.126,10e-4 -0.254,0.02 -0.383,0.022 -0.219,0.005 -0.472,0.078 -0.477,-0.401 -0.004,-0.379 0.215,-0.354 0.396,-0.367 1.3519996,-0.1 2.6689996,-0.449 3.9899996,-0.782 2.537,-0.639 5.097,-1.157 7.623,-1.854 2.423,-0.668 4.826,-1.45 7.219,-2.274 3.224,-1.111 6.421,-2.319 9.595,-3.658 3.815,-1.609 7.594,-3.332 11.303,-5.298 5.11,-2.706 10.1222,-5.701 14.9824,-9.118 6.6268,-4.658 12.9945,-9.84 18.9634,-15.891 3.7102,-3.762 7.2645,-7.746 10.6158,-12.072 1.9311,-2.491 3.7431,-5.126 5.5353,-7.802 3.283404,-4.903 6.247504,-10.142 8.879804,-15.718 1.9758,-4.188 3.7217,-8.572 5.2473,-13.123 2.3007,-6.8592 4.0933,-13.9647 5.3284,-21.3632 0.7278,-4.3622 1.1192,-8.8043 1.5374,-13.2449 0.063,-0.5492 0.0351,-1.1848 -0.642,-0.9744 z"}
                    fill={"#00b0a9"}
                    fillOpacity={0.25}
                />
            </Svg>
        )
    }
}

const styles = StyleSheet.create({
    rootStyle: {
        display: "flex",
        flexDirection: "column",
        flexBasis: 1,
        flexWrap: "nowrap",
        flexGrow: 1,
    },
    goBackButtonContainer: {
        flexBasis: "auto",
        margin: 4,
    },
    actionBarStyle: {
        backgroundColor: "#cceae9",
    },
    addContentFabStyle: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        backgroundColor: "#5a72db",
    },
    listStackContainerStyle: {
        width: "100%",
        height: "100%",
        display: "flex",
        flex: 1,
    },
    listStyle: {
        position: "absolute",
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
        overflow: "scroll",
        //backgroundColor: "red",
        elevation: 1,
        zIndex: 2
    },
    listBackgroundEmptyHintStyle: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        flexDirection: "column",

        //zIndex: 1,
       // backgroundColor: "blue",
        position: "absolute",
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
    },
    listBackgroundRefreshHintStyle: {
        position: "absolute",
        left: 0,
        top: 0,
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
    },
    cardStyle: {
        margin: 0,
    }
})

export default HomeScreenFragment;
