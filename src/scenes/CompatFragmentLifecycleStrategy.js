export default class CompatFragmentLifecycleStrategy {
    constructor(fragment) {
        this.fragment = fragment;
        this.gotoScene = this.gotoScene.bind(this);
    }

    gotoScene(name: string, params: object) {
        this.fragment.history.push('/' + name);
    }

    dispatchFragmentDidMount() {
        return false;
    }

    dispatchFragmentWillUnmount() {
        return false;
    }

}
