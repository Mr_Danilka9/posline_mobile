import React, {Component} from 'react';
import {View, StyleSheet, Text, Platform, StatusBar} from 'react-native'

export default class SplashScene extends Component {
    render() {
        return (
            <React.Fragment>
                <StatusBar backgroundColor={styles.backgroundStyle.backgroundColor}
                           translucent={true}/>
                <View style={styles.backgroundStyle}>
                    <Text style={styles.titleStyle}>PLANKTON</Text>
                </View>
            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    backgroundStyle: {
        display: "flex",
        width: "100%",
        height: "100%",
        backgroundColor: "#2b7a78",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column"
    },
    titleStyle: {
        color: "white",
        fontSize: 36,
        fontFamily: (Platform.OS === 'ios' ? 'Courier' : 'Courier'),
        fontWeight: "600",
    }
})
