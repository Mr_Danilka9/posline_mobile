import React from 'react';
import CompatFragment from "./CompatFragment";
import {View, Text, StatusBar, StyleSheet} from "react-native";
import CompatSafeAreaView from "../widgets/CompatSafeAreaView";
import PdfDocumentView from "../widgets/PdfDocumentView";
import UniqueAppIdContext from "../contexts/UniqueAppIdContext";
import ScreenshotViolationDialog from "./ScreenshotViolationDialog";
import ScreenCaptureProtectionModule from "../modules/ScreenCaptureProtectionModule";
import {Button, Dialog, Paragraph, Portal} from "react-native-paper";

export default class PDFViewFragment extends CompatFragment {

    static contextType = UniqueAppIdContext

    constructor(props, context, updater) {
        super(props, context, updater);
        this.onScreenshotEvent = this.onScreenshotEvent.bind(this);
        this.onStopShowContentRequest = this.onStopShowContentRequest.bind(this);
        this.onNoInternetConnectionDialogDismiss = this.onNoInternetConnectionDialogDismiss.bind(this);
        this.screenCaptureProtectionModule = new ScreenCaptureProtectionModule();
        // this.getScreenCaptureLockButtonTitle = this.getScreenCaptureLockButtonTitle.bind(this);
        // this.onScreenCaptureLockToggled = this.onScreenCaptureLockToggled.bind(this);
        this.state = {
            screenCaptureLocked: false,
            noInternetConnectionShown: false,
            appId: this.context,
            titleStr: this.props.route.params.title,
            contentId: this.props.route.params.item.id,
            errorStr: null,
            screenshotViolationShown: false
        };
    }

    onCreate() {
        //this.fetchUniqueAppInstanceId = this.fetchUniqueAppInstanceId.bind(this);
        console.log("code: " + this.props.route.params.codeValue);
        this.setState({
            screenCaptureLocked: false,
            appId: this.context,
            titleStr: this.props.route.params.title,
            contentId: this.props.route.params.item.id,
            errorStr: null,
            screenshotViolationShown: false,
        });
        const {setOptions} = this.props.navigation;
        setOptions({
            title: this.state.titleStr
        });
        console.log("title " + this.state.titleStr)
        console.log(this.state.appId)
        this.screenCaptureProtectionModule.enableScreenCaptureProtection(this.onScreenshotEvent, this.onStopShowContentRequest);
    }

    onNoInternetConnectionDialogDismiss() {
        this.screenCaptureProtectionModule.enableScreenCaptureProtection(this.onScreenshotEvent, this.onStopShowContentRequest);
        this.setState({
            noInternetConnectionShown: false,
        })
    }

    onStopShowContentRequest(event) {
        this.setState({
            noInternetConnectionShown: true,
        })
        this.screenCaptureProtectionModule.disableScreenCaptureProtection();
    }


    onScreenshotEvent(event) {
        switch (event) {
            default: {
                this.setState({
                    screenshotViolationShown: true,
                })
            }
        }
    }

    onPause() {
        this.screenCaptureProtectionModule.disableScreenCaptureProtection();
    }

    render() {
        //const source = {uri: "http://www.africau.edu/images/default/sample.pdf", cache: true};
        let uri = "http://api.plankton.posline.org/api/app/getContent/?appToken=" + this.state.appId + "&id=" + this.state.contentId
        // 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf'
        const source = {
            uri: uri,
            cache: true
        };
        if (this.state.noInternetConnectionShown) {
            return this.renderNoInternetConnection();
        }
        if (this.state.errorStr != null) {
            return this.renderError();
        }
        if (this.state.appId == null) {
            return this.renderNoAppId();
        }
        return this.renderPdf(source);
    }

    renderError() {
        return (
            <React.Fragment>
                <StatusBar translucent={true}/>
                {/*<CompatSafeAreaView style={styles.container}>*/}
                <Text>Unable to find document</Text>
                {/*</CompatSafeAreaView>*/}
            </React.Fragment>
        )
    }

    renderNoInternetConnection() {
        return (
            <React.Fragment>
                <Portal>
                    <Dialog visible={this.state.isVisible}
                            dismissable={true}>
                        <Dialog.Title>{"No internet connection"}</Dialog.Title>
                        <Dialog.Content style={styles.contentContainerStyle}>
                            <Paragraph style={styles.descriptionStyle}>{"Позязя включите интернет обратно"}</Paragraph>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button onPress={this.onNoInternetConnectionDialogDismiss}>OK</Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </React.Fragment>
        )
    }

    renderNoAppId() {
        return (
            <React.Fragment>
                <StatusBar translucent={true}/>
            </React.Fragment>
        )
    }

    renderPdf(source) {
        console.log(source.uri);
        return (
            <React.Fragment>
                <StatusBar translucent={true}/>
                {/*<CompatSafeAreaView style={styles.container}>*/}
                <View style={styles.pdfViewContainer}>
                    {/*    <Text style={{margin: 0, backgroundColor: "red"}}>PDF</Text>*/}
                    <PdfDocumentView
                        source={source}
                        onLoadComplete={(numberOfPages, filePath) => {
                            console.log(`number of pages: ${numberOfPages}`);
                        }}
                        onPageChanged={(page, numberOfPages) => {
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error) => {
                            this.setState({
                                errorStr: error
                            })
                            console.log(error);
                        }}
                        onPressLink={(uri) => {
                            console.log(`Link pressed: ${uri}`)
                        }}
                        style={styles.pdfView}/>
                </View>
                <ScreenshotViolationDialog
                    onDismissCallback={() => this.setState({screenshotViolationShown: false})}/>
                {/*<Button*/}
                {/*    title={this.getScreenCaptureLockButtonTitle()}*/}
                {/*    onPress={this.onScreenCaptureLockToggled}/>*/}
                {/*</CompatSafeAreaView>*/}
            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        //backgroundColor: "blue",
        width: "100%",
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    pdfViewContainer: {
        //backgroundColor: "yellow",
        width: "100%",
        height: "100%",
        flex: 1,
        //flex: 1,
        // justifyContent: 'flex-start',
        // alignItems: 'center',
        //marginTop: -25,
    },
    pdfView: {
        //backgroundColor: "blue",
        //flex: 1,
        width: "100%",
        //width: Dimensions.get('window').width,
        //height: Dimensions.get('window').height,
        height: "100%",
    }
})
