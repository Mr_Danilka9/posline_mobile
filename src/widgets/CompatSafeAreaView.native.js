import React, {Component} from "react";
import {SafeAreaView} from "react-native-safe-area-context";

class CompatSafeAreaView extends Component {
    constructor(props, context, updater) {
        super(props, context, updater);
    }
    render() {
        return (
            <SafeAreaView style={this.props.style}>
                {this.props.children}
            </SafeAreaView>
        )
    }
}

export default CompatSafeAreaView;
