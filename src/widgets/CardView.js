import React, {Component} from "react";
import {Card, withTheme} from "react-native-elements";
import {StyleSheet} from "react-native";
import './CardView.web.css'

class CardView extends Component {
    render() {
        const {theme, updateTheme, replaceTheme} = this.props;
        return (
            <Card title={this.props.title}
                  style={cardStyles.cardStyle}
                  containerStyle={cardStyles.cardContainerStyle}>
                {this.props.children}
            </Card>
        )
    }
}

const cardStyles = StyleSheet.create({
    cardContainerStyle: {
        marginHorizontal: 8,
        marginTop: 4,
        marginBottom: 4,
        padding: 8,
        elevation: 0,
        borderRadius: 4,
        borderColor: "transparent",
        boxShadow: "rgba(0, 0, 0, 0.25) 0px 1px 3px",
    },
    cardStyle: {
        borderRadius: 4,
        padding: 4,
    }
})

export default withTheme(CardView);
