import React, {Component} from "react";
import {Card} from "react-native-elements";
import {StyleSheet} from "react-native";

class CardView extends Component {
    render() {
        return (
            <Card title={this.props.title}
                  style={cardStyles.cardStyle}
                  containerStyle={cardStyles.cardContainerStyle}>
                {this.props.children}
            </Card>
        )
    }
}

const cardStyles = StyleSheet.create({
    cardContainerStyle: {
        margin: 4,
        elevation: 2,
        borderRadius: 8,
        borderColor: "transparent",
    },
    cardStyle: {
        borderRadius: 4,
        padding: 4,
    }
})

export default CardView;
