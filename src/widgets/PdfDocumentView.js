import React, {Component} from "react";
import {Document} from "react-pdf";

export default class PdfDocumentView extends Component {
    render(): * {
        // return React.createElement(Document, {
        //     file: this.props.source.uri,
        //     ...this.props
        // }, null);
        return (
            <Document
                file={this.props.source.uri}/>
        )
    }
}
