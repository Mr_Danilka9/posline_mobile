import React, {Component} from "react";
import {View} from "react-native";

class CompatSafeAreaView extends Component {
    constructor(props, context, updater) {
        super(props, context, updater);
    }

    render() {
        return <View style={this.props.style}>
            {this.props.children}
        </View>
    }
}

export default CompatSafeAreaView;
