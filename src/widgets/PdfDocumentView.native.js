import React, {Component} from "react";
import Pdf from "react-native-pdf";

export default class PdfDocumentView extends Component {
    render() {
        return React.createElement(Pdf, {
            ...this.props
        })
        // return (
        //     <Pdf
        //         source={{
        //             uri: this.props.sourceUri,
        //             cache: this.props.sourceUseCache,
        //         }}
        //         onLoadComplete={this.props.onLoadComplete}
        //         onPageChanged={this.props.onPageChanged}
        //         onError={this.props.onError}
        //     />
        // )
    }
}
