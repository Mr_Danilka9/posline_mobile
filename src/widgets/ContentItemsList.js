import React, {Component} from "react";
import {FlatList, Text, View} from "react-native";
import {Avatar, Button, Card, Headline, Paragraph, Subheading} from 'react-native-paper';
import {StyleSheet} from "react-native";
import LocaleStrings from "../localization"

export default class ContentItemsList extends Component {
    render() {
        return (
            <FlatList
                //numColumns={1}
                onRefresh={this.props.onRefreshCallback}
                refreshing={this.props.refreshing}
                style={styles.listStyle}
                data={this.props.listData}
                renderItem={({item}) =>
                    <MainListItem
                        item={item}
                        itemCallbacks={this.props.itemCallbacks}
                    />
                }/>
        )
    }
}

function MainListItem({item, itemCallbacks}) {
    let clickCallback = () => {
        itemCallbacks.onListItemClicked(item);
    }
    return (
        <Card
            elevation={2}
            style={{
                display: "flex",
                flexDirection: "column",
                margin: 8,
                alignItems: "center",
                padding: 8,
            }}>
            <View style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                padding: 8
            }}>
                <Avatar.Icon size={56} icon={"file"} style={{backgroundColor: "#5a72db"}}/>
            </View>
            <Headline style={{
                textAlign: "center"
            }}>{"" + item.name}</Headline>
            <Subheading style={{
                textAlign: "center"
            }}>{"" + item.description}</Subheading>
            <Paragraph style={{
                textAlign: "center"
            }}>{LocaleStrings.ContentList.Author+": " + item.author}</Paragraph>
            <Card.Actions style={{
                display: "flex",
                alignItems: "center",
                //backgroundColor: "red",
                width: "100%"
            }}>
                <Button
                    style={{
                        width: 192,
                        padding: 0,
                        fontSize: 12,
                        maxHeight: 32,
                        borderRadius: 16
                    }}
                    contentStyle={{
                        backgroundColor: "#2b7a78",
                        maxHeight: 32,
                        margin: 0,
                        padding: 0,
                    }}
                    mode={"contained"}
                    onPress={clickCallback}>{LocaleStrings.ContentList.Open}</Button>
            </Card.Actions>
        </Card>
    )
}

const styles = StyleSheet.create({
    listStyle: {
        //backgroundColor: "#ff000088",
        overflow: "scroll",
    }
})
