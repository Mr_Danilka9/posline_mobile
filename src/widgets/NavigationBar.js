import React, {Component} from "react";
import {
    Text,
} from 'react-native';

class NavigationBar extends Component {
    render(): React.ReactNode {
        return (
            <>
                <Text>Im a navigation bar with icons</Text>
            </>
        )
    }
}

export default NavigationBar;
