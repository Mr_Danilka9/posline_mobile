import React, {Component} from 'react';
import RootNavigationRouter from './routing/RootNavigationRouter'
import {Portal} from 'react-native-paper';
import UniqueAppIdContext, {asyncGetUniqueAppId} from './contexts/UniqueAppIdContext'
import {StyleSheet,} from 'react-native';
import SplashScene from "./scenes/SplashScene";

// export default class App extends Component {
//   render(): React.ReactNode {
//     return (
//         <Text>
//           Test
//         </Text>
//     )
//   }
// }

const Theme = {
    Button: {
        raised: true,
        //color: "#000000",
        elevation: 8,
        titleStyle: {
            color: 'white'
        }
    }
}

class App extends Component {

    constructor(props, context, updater) {
        super(props, context, updater);
        this.state = {
            appId: null
        }
        // try {
        //     (async () => {
        //         this.setState({
        //             appId: getUniqueAppId()
        //         })
        //     })()
        // } catch (e) {
        //     console.log(e)
        // }
    }

    componentDidMount(): void {
        asyncGetUniqueAppId((appId) => {
            this.setState({
                appId: appId
            })
        })
    }

    render() {
        if (this.state.appId === null) {
            return this.renderStub();
        } else {
            return this.renderApp();
        }
    }

    renderStub() {
        return <SplashScene/>
    }

    renderApp() {
        return (<>
                {/*<StatusBar backgroundColor="#00000000" translucent/>*/}
                {/*<ThemeProvider theme={Theme}>*/}
                    <UniqueAppIdContext.Provider value={this.state.appId}>
                        <Portal.Host>
                            <RootNavigationRouter/>
                        </Portal.Host>
                    </UniqueAppIdContext.Provider>
                {/*</ThemeProvider>*/}
            </>
        )
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: "#EEEEEE",
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: "#FFFFFF",
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: "#000000",
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: "#333333",
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: "#222222",
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});

export default App;
