import openSocket from 'socket.io-client';

const baseSocketPath = "http://api.plankton.posline.org/app/websocket"

const socket = openSocket(baseSocketPath);
