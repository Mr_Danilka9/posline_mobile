import axios from "axios";

const basePath = "http://api.plankton.posline.org/api/app"

export const registerCode = (appId,code) => axios({
    method: 'POST',
    url: `${basePath}/registerCode/`,
    headers: {
        "Content-Type": "application/json"
    },
    data:{
        appToken: appId,
        contentToken: code,
    }
})

export const getCodeContent = (appId,code) => axios({
    method: 'GET',
    url: `${basePath}/getContent/?appToken=${appId}`,
    headers: {
        "Content-Type": "application/json"
    }
})

export const getLibraryContent = (appId) => axios({
    method: 'GET',
    url: `${basePath}/getUserContent/?appToken=${appId}`,
    headers: {
        "Content-Type": "application/json"
    }
})
